package com.dasnano.vddocumentcapture;

import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.CallbackContext;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.os.Handler;
import android.util.Log;

import com.dasnano.vddocumentcapture.config.VDDocumentConfiguration;
import com.dasnano.vddocumentcapture.other.VDDocument;
import com.dasnano.vddocumentcapture.other.VDEnums;
import com.dasnano.vddocumentcapture.VDDocumentCapture;

import java.io.ByteArrayInputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * This class echoes a string called from JavaScript.
 */
public class plugincordova extends CordovaPlugin implements VDDocumentCapture.IVDDocumentCapture{

    @Override
    public boolean execute(String action, JSONArray args, CallbackContext callbackContext) throws JSONException {
        if (action.equals("coolMethod")) {
            String message = args.getString(0);
            this.coolMethod(message, callbackContext);

            if (!VDDocumentCapture.isStarted()) {
                List<String> spainDocs = new ArrayList<>();
                spainDocs.add("ES_IDCard_2006");
                spainDocs.add("ES_IDCard_2015");
                Map<String, String> configuration = new HashMap<>();
                configuration.put(VDDocumentConfiguration.CLOSE_BUTTON, "YES");
                VDDocumentCapture.startWithDocumentIDs(this, this.cordova.getActivity().getApplicationContext(), spainDocs, configuration);
                String version = VDDocumentCapture.getVersion();
            }

            return true;
        }
        return false;
    }

    private void coolMethod(String message, CallbackContext callbackContext) {
        if (message != null && message.length() > 0) {
            callbackContext.success(message);
        } else {
            callbackContext.error("Expected one non-empty string argument.");
        }
    }

    @Override
    public void VDDocumentCaptured(ByteArrayInputStream byteArrayInputStream, VDEnums.VDCaptureType vdCaptureType, List<VDEnums.VDDocumentType> list) {
        Log.i("document","document captured");
    }

    @Override
    public void VDDocumentCutCaptured(ByteArrayInputStream byteArrayInputStream, VDEnums.VDCaptureType vdCaptureType, List<VDEnums.VDDocumentType> list) {
        Log.i("document","document cut captured");
    }

    @Override
    public void VDDocumentCaptured(byte[] bytes, VDEnums.VDCaptureType vdCaptureType, List<VDDocument> list) {
        Log.i("document","document captured 2");
    }

    @Override
    public void VDDocumentCutCaptured(byte[] bytes, VDEnums.VDCaptureType vdCaptureType, List<VDDocument> list) {
        Log.i("document","document ut captured 2");
    }

    @Override
    public void VDTimeWithoutPhotoTaken(int i, VDEnums.VDCaptureType vdCaptureType) {
        Log.i("document","time without photo taken");
    }

    @Override
    public void VDDocumentCaptureFinished(boolean b) {
        Log.i("document","capture finished");
    }
}
